const crypto = require('crypto');

module.exports = {
    generatePasword: (password, salt) => {
        return new Promise((resolve, reject) => {
            resolve(crypto.createHmac('sha256', password + salt).digest('hex'));
        });
    }
}