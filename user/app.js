// gRPC config
const PROTO_PATH = __dirname + './../protos/users.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
const user_proto = grpc.loadPackageDefinition(packageDefinition).users;

// Mongo config
const mongo = require("mongodb");
const mongoClient = mongo.MongoClient;
const url = "mongodb://" + (
  process.env.MONGO_URL || "localhost") + ":" + (
    process.env.MONGO_PORT || "27017") + "/" + (
      process.env.MONGO_DB || "vmadb");

// Tools config
const tools = require('./tools');

function userGet(call, callback) {
  mongoClient.connect(url, (err, db) => {
    if (err) throw err;

    if (Buffer.byteLength(call.request.user_id, "utf8") != 24)
      callback(null, {
        status_code: 400,
        service_message: "ID value should be a valid one.", 
        user_data: JSON.stringify({})
      });
    else{
      db.collection("users").findOne({
        "_id": new mongo.ObjectID(call.request.user_id)}, 
        (err, res) => {
          if (err) throw err; 
          callback(null, {
            status_code: res? 200 : 404, 
            service_message: res? "User data retrived." : "User data not found.", 
            user_data: JSON.stringify(res || {})});
          db.close();
      });
    }
  });
}

function userCreate(call, callback) {
  mongoClient.connect(url, (err, db) => {
    if (err) throw err;

    if(!call.request.name || 
      !call.request.age || 
      !call.request.email || 
      !call.request.password || 
      !call.request.address){
        callback(null, {
          status_code: 400,
          service_message: "Missing form arguments.",
          user_data: ""
        });
      }else{
        tools.generatePasword(call.request.password, call.request.email).then((new_password) => {
          db.collection("users").insertOne({
            "name": call.request.name,
            "age": call.request.age,
            "email": call.request.email,
            "password": new_password,
            "address": call.request.address
          }, (err, result) => {
            callback(null, {
              status_code: 200, 
              service_message: "User created.", 
              user_data: JSON.stringify({
                "_id": result.insertedId })
            });
            db.close();
          });
        });
      }
  });
}

function userUpdate(call, callback) {
  mongoClient.connect(url, (err, db) => {
    if (err) throw err;

    if (Buffer.byteLength(call.request.user_id, "utf8") != 24)
      callback(null, {
        status_code: 400,
        service_message: "ID value should be a valid one.", 
        user_data: JSON.stringify({})
      });
    else{
      updateData = {
        "name": call.request.name,
        "age": call.request.age,
        "email": call.request.email,
        "password": call.request.password,
        "address": call.request.address
      };

      if((updateData.password && !updateData.email) || (updateData.email && !updateData.password))
        callback(null, {
          status_code: 400,
          service_message: "To update email/password, you should confirm those informations.",
          user_data: JSON.stringify({})
        });
      else{
        if(updateData.password && updateData.email){
          tools.generatePasword(updateData.password, updateData.email).then((new_password) => {
            for (item in updateData){
              if(!updateData[item])
                delete updateData[item];
            }

            updateData.password = new_password;

            db.collection("users").updateOne({
              "_id": new mongo.ObjectID(call.request.user_id)
            }, { $set: updateData }, (err, res) => {
              callback(null, {
                status_code: res.modifiedCount > 0? 200 : 404,
                service_message: res.modifiedCount > 0? "User updated." : "User dont exist.",
                user_data: ""
              });
              db.close();
            });
          });
        }else{
          for (item in updateData){
            if(!updateData[item])
              delete updateData[item];
          }

          db.collection("users").updateOne({
            "_id": new mongo.ObjectID(call.request.user_id)
          }, { $set: updateData }, (err, res) => {
            callback(null, {
              status_code: res.modifiedCount > 0? 200 : 404,
              service_message: res.modifiedCount > 0? "User updated." : "User dont exist.",
              user_data: ""
            });
            db.close();
          });
        }
      }
    }
  });
}

function userDelete(call, callback) {
  mongoClient.connect(url, (err, db) => {
    if (err) throw err;

    if (Buffer.byteLength(call.request.user_id, "utf8") != 24)
      callback(null, {
        status_code: 400,
        service_message: "ID value should be a valid one.", 
        user_data: JSON.stringify({})
      });
    else{
      db.collection("users").deleteOne(
        {"_id": new mongo.ObjectID(call.request.user_id)},
        (err, res) => {
          callback(null, {
            status_code: res.deletedCount > 0? 200 : 404,
            service_message: res.deletedCount > 0? "User deleted." : "User dont exist.",
            user_data: ""});
            db.close();
        }
      );
    }
  });
}

function main() {
  var server = new grpc.Server();
  server.addService(user_proto.UserGetter.service, {userGet: userGet});
  server.addService(user_proto.UserCreater.service, {userCreate: userCreate});
  server.addService(user_proto.UserUpdater.service, {userUpdate: userUpdate});
  server.addService(user_proto.UserDeleter.service, {userDelete: userDelete});
  server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  server.start();
}

main();
