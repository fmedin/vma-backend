# VMA backend

### TODO

* tests
* error handler when some service are offline
* check duplicate email in user create service 

### The project

The project is running with two services: api and user.

- **api**: Used to handle all the requests and communicate with private services.
- **user**: Used to handle with all user process.

Their communication are made with [gRPC](http://grpc.io/). So, protos folder handle with all kind of protos files.

### How to run

>  Running directly

If you want to run directly from your computer, you should ```$ npm install``` in each service folder and then run `app.js` file with node:
```sh
$ npm i
$ node app.js
```
<br />

> Running from Docker

If you want to run with docker. First you should build the base image in root folder:
```sh
$ docker build -t vma/base .
```
After built, just run with compose:
```sh
$ docker-compose up --build
```
> each update in protos, the base build process should happen again.