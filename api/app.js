// Init config
const express = require('express');
const formidable = require('express-formidable');
const app = express();
app.use(formidable());

// gRPC config
const PROTO_PATH = __dirname + './../protos/users.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH, {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const user_proto = grpc.loadPackageDefinition(packageDefinition).users;

const user_service = (
    process.env.USER_SERVICE_URL || "localhost") + ":" + (
        process.env.USER_SERVICE_PORT || "50051")

app.get('/', function (req, res) {
    res.send('Users API v1');
});

app.get('/users/:user_id', (req, res) => {
    var client = new user_proto.UserGetter(
        user_service,
        grpc.credentials.createInsecure());

    client.userGet({
        user_id: req.params.user_id
    }, (err, response) => {
        response.user_data = JSON.parse(response.user_data);
        res.send(response);
    });
});

app.post('/users', (req, res) => {
    var client = new user_proto.UserCreater(
        user_service,
        grpc.credentials.createInsecure());

    client.userCreate({
        name: req.fields.name,
        age: req.fields.age,
        email: req.fields.email,
        password: req.fields.password,
        address: req.fields.address
    }, (err, response) => {
        if(response.status_code == 200)
            res.send(JSON.parse(response.user_data));
        else
            res.send(response);
    });
});

app.put('/users', (req, res) => {
    var client = new user_proto.UserUpdater(
        user_service,
        grpc.credentials.createInsecure());

    client.userUpdate({
        user_id: req.fields.user_id,
        name: req.fields.name,
        age: req.fields.age,
        email: req.fields.email,
        password: req.fields.password,
        address: req.fields.address
    }, (err, response) => {
        res.send(response)
    });
});

app.delete('/users', (req, res) => {
    var client = new user_proto.UserDeleter(
        user_service,
        grpc.credentials.createInsecure());

    client.userDelete({
        user_id: req.fields.user_id
    }, (err, response) => {
        res.send(response);
    });
});

app.listen(3000, function () {
    console.log('VMA API listening on port 3000');
});